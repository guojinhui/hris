<?php
/**
 * config.php
 * HRIS 接口对接配置文件
 * @author Deepseath
 * @version $Id$
 */
use think\facade\Env;

return [
    // 分配到的原始 TOKEN
    'appKey' => Env::get('hris.originToken', ''),
    // HRIS 接口 URL 前缀
    'baseUrl' => Env::get('hris.baseUrl', ''),
    // 调试模式
    'debug' => Env::get('hris.debug', false),
    // 一级部门名称（中文）
    'orgUnitZH' => Env::get('hris.orgUnitZH', ''),
    // 一级部门名称（英文）
    'orgUnitEN' => Env::get('hris.orgUnitEN', '')
];
