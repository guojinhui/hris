<?php
/**
 * Mapping.php
 * 接口字典，转义字段 v2
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\hris\ApiV2;

use Symfony\Contracts\Cache\ItemInterface;

class Mapping
{
    /**
     * 基类服务对象
     * @var \deepseath\hris\Hris
     */
    protected $service = null;

    public function __construct(\deepseath\hris\Hris $service)
    {
        $this->service = $service;
    }

    

    /**
     * 获取指定字段的字典信息
     *
     * @param string $field
     * @param boolean $force
     * @return array
     */
    public function get(string $field = null, bool $force = false) : array
    {
        if ($field !== null) {
            $type = $this->types($field);
            if (is_array($type)) {
                return $type;
            }
        }

        if ($force !== false) {
            $mappings = $this->_getWithoutCache();
        } else {
            $cache = $this->service->cache();
            $mappings = $cache->get('hris_mappings', function (ItemInterface $item) {
                $mappings = $this->_getWithoutCache();
                $item->expiresAfter($this->service->config['mapping_expire']);
                return $mappings;
            });
            unset($cache);
        }

        return $field === null ? $mappings : $mappings[$type];
    }

    /**
     * 无缓存获取 mappings
     * @return array
     */
    private function _getWithoutCache() : array
    {
        $result = $this->service->apiRequest('get', 'mappingVO', []);
        $mappings = [];
        foreach ($result['mappings'] ?? [] as $mapping) {
            $mappings[$mapping['mappingType']][$mapping['mappingId']] = $mapping['mappingValue'];
        }
        unset($result, $mapping);
        return $mappings;
    }

    /**
     * 列出字典字段
     *
     * @param string|null $field
     * @return mixed
     */
    public function types(string $field = null) : mixed
    {
        $fieldMap = [
            'employeeStatus' => [1 => '在职', 3 => '离职'],
            'entityId' => 'entityIds',
            'locationId' => 'locations',
            'jobRole' => 'jobRoles',
            'employeeType' => 'employeeTypes',
            'gradeId' => 'grades',
            'nation' => 'nations',
            'nationality' => 'nationalities',
            'gender' => 'genders',
            'certificateType' => 'certificateTypes',
            'maritalStatus' => 'maritalStatuses',
            'highestEducationId' => 'highestEducations',
            'residencyProvinceId' => 'provinces',
            'residencyCityId' => 'cities',
            'residencyType' => 'residencyTypes',
            'residencyType2' => 'residencyTypes2',
            'emergency.relationshipId' => 'relationshipIds',
            'bankId' => 'banks',
            'recruitment' => 'recruitments',
            'recruitmentHc' => 'recruitmentHcs',
            'esop' => 'esop',
            'leaveCompensate' => 'leaveCompensate',
            'hireAgain' => 'hireAgain'
        ];

        if ($field === null) {
            return $fieldMap;
        }

        if (!isset($fieldMap[$field])) {
            throw new \Exception('Hris sdk error: mapping type not exists', 10009);
        }

        return $fieldMap[$field];
    }

}
