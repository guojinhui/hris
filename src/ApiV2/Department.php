<?php
/**
 * Department.php
 * 部门接口V2
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\hris\ApiV2;

class Department
{
    /**
     * 基类服务对象
     * @var \deepseath\hris\Hris
     */
    protected $service = null;

    /**
     * 部门删除状态：正常（未删除）
     * @var integer
     */
    const DELETE_NO = 1;
    /**
     * 部门删除状态：已删除
     * @var integer
     */
    const DELETE_YES = 2;

    /**
     * 畅移本地顶级部门（智慧零售部）的 id
     * md5('2022-04-27 17:00:01') —— 该值没具体意义主要就是一个在hris识别“智慧零售事业部”的标识<br>
     * 该值的来源是，首次导给 hris 全量部门数据时，以当时时间md5做顶级部门标识，方便 hris 导入处理
     * @var string
     */
    const CY_TOP_ID = '6e691aaf7d16f7b5199cce086e781368';

    public function __construct(\deepseath\hris\Hris $service)
    {
        $this->service = $service;
    }

    /**
     * 获取畅移在集团内的架构信息
     *
     * @return array
     */
    public function getCyBranch() : array
    {
        foreach ($this->branchList() as $branch) {
            if (isset($branch['branchId']) && isset($branch['branchNo']) && $branch['branchNo'] == self::CY_TOP_ID) {
                return [
                    'branchId' => $branch['branchId'],
                    'branchNo' => $branch['branchNo'],
                    'nameEN' => $branch['nameEN'],
                    'nameZH' => $branch['nameZH']
                ];
                break;
            }
        }

        throw new \Exception('无法获取畅移在集团内的架构信息', 91000);
    }

    /**
     * 获取部门信息
     * @desc 获取部门及部门上下级关系
     * @param array $params
     * @return array
     */
    public function branchList(array $params = []) : array
    {
        $params = array_merge([], $params);

        $result = $this->service->apiRequest('get', 'branchTree', $params);
        return $result['branchTree'] ?? [];
    }

    /**
     * 新增部门
     * 
     * @desc 新增部门，成功则返回 hris 该部门的 ID
     * @param array $params
     * <pre>
     *     branchNo         薪人薪事部门 ID
     *     parentBranchId   上级部门 ID（HRIS 部门 id）
     *     nameEN           部门英文名
     *     nameZH           部门中文名
     * </pre>
     * @return int
     */
    public function branchAdd(array $params) : array
    {
        $params = array_merge($params, [
            'deleted' => self::DELETE_NO
        ]);

        $result = $this->service->apiRequest('post', 'branch_add_edit', $params);
        if (is_scalar($result)) {
            if (preg_match('/ID(\d+)/is', $result, $match)) {
                // 新增部门ID56896新增部门
                $branchId = $match[1];
            }
        } else {
            $branchId = $result['branchId'];
        }
        return [
            'branchId' => $branchId
        ];
    }

    /**
     * 编辑部门
     *
     * @param array $params
     * <pre>
     *     branchId         待编辑的部门 ID（HRIS 部门 id）
     *     branchNo         薪人薪事部门 ID
     *     parentBranchId   上级部门 ID（HRIS 部门 id）
     *     nameEN           部门英文名
     *     nameZH           部门中文名
     * </pre>
     * @return boolean
     */
    public function branchEdit(array $params) : bool
    {
        $params = array_merge($params, [
            'deleted' => self::DELETE_NO
        ]);

        $result = $this->service->apiRequest('post', 'branch_add_edit', $params);
        if (is_scalar($result)) {
            return true;
        }
        return false;
    }

    /**
     * 删除部门
     *
     * @param array $params
     * <pre>
     *     branchId         待删除的部门 ID（HRIS 部门 id）
     *     branchNo         薪人薪事部门 ID
     *     parentBranchId   上级部门 ID（HRIS 部门 id）
     *     nameEN           部门英文名
     *     nameZH           部门中文名
     * </pre>
     * @return boolean
     */
    public function branchDelete(array $params) : bool
    {
        $params = array_merge($params, [
            'deleted' => self::DELETE_YES
        ]);

        $result = $this->service->apiRequest('post', 'branch_add_edit', $params);
        if (is_scalar($result)) {
            return true;
        }
        return false;
    }
}
