<?php
/**
 * User.php
 * 用户接口 v2
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\hris\ApiV2;

class User
{
    /**
     * 基类服务对象
     * @var \deepseath\hris\Hris
     */
    protected $service = null;

    public function __construct(\deepseath\hris\Hris $service)
    {
        $this->service = $service;
    }

    /**
     * 员工基础信息
     * @desc 查询 HRIS 中的员工基础信息，参数为空返回所有成员
     * @param array $params
     * <pre>
     *      nameZH      按中文名模糊查询
     *      email       按邮箱查询
     * </pre>
     * @return array
     * <pre>
     *  list
     *   employeeId
     *   nameZH
     *   nameEN
     *   email
     *   wxUserId
     *   positionId
     *   remark1
     * </pre>
     */
    public function list(array $params = []) : array
    {
        $params = array_merge([
            //'nameZH' => '',
            //'email' => ''
        ], $params);

        $result = $this->service->apiRequest('get', 'employeeList', $params);
        return $result['employeeList'] ?? [];
    }

    /**
     * 新增员工
     * @desc 向HRIS中插入新用户
     * @param array $params
     * @return mixed
     */
    public function add(array $params) : mixed
    {
        if (!empty($params['email']) && preg_match('/@i\-click\.com$/is', $params['email'])) {
            // 如果是 i-click.com 邮箱则不处理
            throw new \Exception('i-click.com mail ignore', 4013);
        }
        $this->_fieldCheck($params);
        // $result;
        return $this->service->apiRequest('post', 'add_employee', $params);
    }

    /**
     * 更新员工
     * @desc 用户修改
     * @param array $params
     * @return mixed
     */
    public function update(array $params) : mixed
    {
        if (!empty($params['email']) && preg_match('/@i\-click\.com$/is', $params['email'])) {
            // 如果是 i-click.com 邮箱则不处理
            throw new \Exception('i-click.com mail ignore', 4013);
        }
        $this->_fieldCheck($params);
        return $this->service->apiRequest('post', 'edit_employee', $params);
    }

    /**
     * 更改邮箱专用接口
     *
     * @param array $params
     * <pre>
     * + employeeId
     * + email
     * + updateEmail
     * </pre>
     * @return mixed
     */
    public function email(array $params) : mixed
    {
        if (!empty($params['email']) && preg_match('/@i\-click\.com$/is', $params['email'])) {
            // 如果是 i-click.com 邮箱则不处理
            throw new \Exception('i-click.com mail ignore', 4013);
        }
        return $this->service->apiRequest('post', 'update_employee_email', $params);
    }

    /**
     * 员工离职
     * @param array $params
     * @param string $newEmail &离职后的 hris 成员邮箱
     * @return mixed
     */
    public function dismiss(array $params, &$newEmail = '') : mixed
    {
        if (!empty($params['email']) && preg_match('/@i\-click\.com$/is', $params['email'])) {
            // 如果是 i-click.com 邮箱则不处理
            throw new \Exception('i-click.com mail ignore', 4013);
        }
        if (isset($params['email']) && !preg_match('/dismiss_\d+_\d+/is', $params['email']) && !preg_match('/delete_\d+_\d+/is', $params['email'])) {
            // 不是离职标识邮箱，则先更改邮箱
            $emailParams = [];
            if (!empty($params['email'])) {
                $emailParams['email'] = $params['email'];
            }
            if (empty($params['email'])) {
                $params['email'] = 'dismiss_'.date('Ymd') .'_' . mt_rand(100000, 999999).'@vchangyi.com';
            } else {
                $params['email'] = $params['email'] . '_' . 'dismiss_'.date('Ymd') .'_' . mt_rand(100000, 999999);
            }
            $newEmail = $params['email'];
            $emailParams['updateEmail'] = $params['email'];
            $this->email($emailParams);
        }

        $params = array_merge($params, [
            'employeeStatus' => self::STATUS_OFFLINE
        ]);
        return $this->service->apiRequest('post', 'edit_employee', $params);
    }

    /**
     * 删除成员
     * @param array $params
     * @param string $newEmail &删除后 hris 成员的邮箱
     * @return mixed
     */
    public function delete(array $params, &$newEmail = '') : mixed
    {
        if (!empty($params['email']) && preg_match('/@i\-click\.com$/is', $params['email'])) {
            // 如果是 i-click.com 邮箱则不处理
            throw new \Exception('i-click.com mail ignore', 4013);
        }
        if (isset($params['email']) && !preg_match('/delete_\d+_\d+/is', $params['email']) && !preg_match('/dismiss_\d+_\d+/is', $params['email'])) {
            $emailParams = [];
            if (!empty($params['email'])) {
                $emailParams['email'] = $params['email'];
            }

            if (empty($params['email'])) {
                $params['email'] = 'delete_'.date('Ymd') .'_' . mt_rand(100000, 999999).'@vchangyi.com';
            } else {
                $params['email'] = $params['email'] . '_' . 'delete_'.date('Ymd') .'_' . mt_rand(100000, 999999);
            }
            $newEmail = $params['email'];
            $emailParams['updateEmail'] = $params['email'];

            $this->email($emailParams);
        }
        
        $params = array_merge($params, [
            'employeeStatus' => self::STATUS_OFFLINE,
            'deleted' => self::IS_DELETE
        ]);
        return $this->service->apiRequest('post', 'edit_employee', $params);
    }

    /**
     * 员工数据参数检查
     * @param array $params
     * @throws \Exception
     * @return bool
     */
    private function _fieldCheck(array $params) : bool
    {

        return true;
    }

    /**
     * 在职状态：在职
     */
    const STATUS_ON = '1';
    /**
     * 在职状态：离职
     */
    const STATUS_OFFLINE = '3';

    /**
     * 是否删除：确认删除
     */
    const IS_DELETE = 2;
    /**
     * 在职状态描述映射关系
     * @var array
     */
    const MAP_STATUS = [
        self::STATUS_ON => '在职',
        self::STATUS_OFFLINE => '离职'
    ];

    /**
     * 法务实体映射关系
     * @var array
     */
    const MAP_ENTOTY = [
        '157' => '畅移（上海）信息科技有限公司',
        '158' => '西安畅展信息科技有限公司',
    ];

    /**
     * 工作地映射关系
     * @var array
     */
    const MAP_LOCATION = [
        '65' => '上海',
        '116' => '西安',
    ];

    /**
     * 员工类型映射关系
     * @var array
     */
    const MAP_TYPE = [
        '0' => '请选择',
        '1' => 'Full Time Employee',// 全职
        '2' => 'Intern',// 实习生
        '3' => 'Consultant',// 顾问
        '4' => 'Others on Payroll',// 编外
        '5' => 'Temp',// 临时工
        '6' => 'Part Time Employee',// 兼职
        '7' => '其他',
    ];

    /**
     * 职级映射关系
     * @var array
     */
    const MAP_GRADE_ID = [
        '179' => ['E2', 'Sr. Executive'],
        '181' => ['E1', 'Executive'],
        '183' => ['M4', 'GM/VP'],
        '187' => ['M3', 'Director'],
        '189' => ['IC6', 'Director'],
        '191' => ['M2', 'Sr. Manager'],
        '193' => ['IC5', 'Sr. Manager'],
        '195' => ['M1', 'Manager'],
        '197' => ['IC4', 'Manager'],
        '199' => ['IC3', 'Associate Manager'],
        '201' => ['IC2', 'Sr. Specialist'],
        '203' => ['IC1', 'Specialist'],
        '205' => ['S2', 'Coordinator'],
        '207' => ['S1', 'Assistant'],
        '209' => ['E1', 'COO'],
        '213' => ['IC7', 'GM/VP'],
        '315' => ['E1', 'CFO'],
        '316' => ['E3', 'Chairman'],
        '319' => ['M5', 'GM/VP']
    ];

    /**
     * 身份证件类型映射关系
     * @var array
     */
    const MP_CERTIFICATE_TYPE = [
        '0' => '请选择',
        '1' => '身份证',
        '2' => '护照',
        '3' => '台胞证',
        '4' => '身份证（香港）',
        '5' => '身份证（新加坡）',
        '6' => '港澳通行证',
    ];

    /**
     * 婚姻状态映射关系
     * @var array
     */
    const MP_MARITAL_STATUS = [
        '0' => '请选择',
        '1' => '未婚',
        '2' => '已婚',
    ];

    /**
     * 最高学历映射关系
     * @var array
     */
    const MAP_HIGHEST_EDUCATION = [
        '0' => ['请选择', ''],
        '95' => ['PhD', '博士'],
        '97' => ['Master', '硕士'],
        '99' => ['Bachelor', '本科'],
        '103' => ['Senior High', '高中'],
        '105' => ['Junior High', '初中'],
        '107' => ['Diploma', '大专'],
        '109' => ['Associate Degree', '大学肆业'],
        '111' => ['Advanced Diploma', '高级文凭'],
        '113' => ['Higher Diploma', '高级文凭'],
        '115' => ['Juris Doctor', '法学博士'],
        '117' => ['Certificate', '证书'],
        '119' => ['Secondary ', '中专'],
        '121' => ['F.5', 'F.5'],
    ];

    /**
     * 民族映射关系
     * @var array
     */
    const NATION = [
        '0' => '请选择',
        '1' => '汉族',
        '10' => '瑶族',
        '11' => '锡伯族',
        '12' => '苗族',
        '2' => '蒙古族',
        '3' => '土家族',
        '4' => '满族',
        '5' => '壮族',
        '6' => '仫佬族',
        '7' => '回族',
        '8' => '英国',
        '9' => '维吾尔族',
    ];

    /**
     * 国籍映射关系
     * @var array
     */
    const MAP_NATION_NALITY = [
        '0' => '请选择',
        '1' => '中国',
        '2' => '美国',
        '3' => '新加坡',
        '5' => '加拿大',
        '7' => '英国',
        '9' => '泰国',
        '11' => '日本',
        '13' => '韩国',
        '15' => '法国',
        '17' => '香港',
        '19' => '台湾',
        '21' => '澳大利亚',
        '23' => '菲律宾',
        '24' => '德国',
        '25' => '意大利',
        '26' => '马来西亚',
        '27' => '新西兰',
        '28' => '西班牙',
        '29' => '其他',
    ];

    /**
     * 角色映射关系
     * @var array
     */
    const MAP_JOB_ROLE = [
        '0' => '请选择',
        '10' => 'Engineering',
        '12' => 'Finance',
        '14' => 'Human Resources',
        '16' => 'Management',
        '17' => 'Marketing',
        '18' => 'Media',
        '19' => 'Optimization',
        '20' => 'Product Management',
        '21' => 'Project Management',
        '23' => 'Sales',
        '28' => 'Creative Design',
        '29' => 'Legal',
        '3' => 'Account Management',
        '30' => 'Product Marketing',
        '31' => 'Product Trainer',
        '32' => 'Sales Operations',
        '37' => 'Solution Planning',
        '4' => 'Accounting',
        '40' => 'Biz Planning & Operations',
        '41' => 'Solution Development',
        '43' => 'Product Operations',
        '44' => 'User Experience Design',
        '45' => 'Public Relations',
        '46' => 'Data Analysis',
        '47' => 'Kol Operations',
        '48' => 'Content Operations',
        '49' => 'Creative',
        '5' => 'Ad. Operations',
        '6' => 'Administration',
        '8' => 'Biz Development',
        '9' => 'Biz Intelligence',
    ];

    /**
     * 性别映射关系
     * @var array
     */
    const MAP_GENDER = [
        '0' => '未填写',
        '1' => '先生',
        '2' => '女士'
    ];
}
