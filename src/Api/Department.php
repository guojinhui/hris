<?php
/**
 * Department.php
 * 部门接口
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\hris\Api;

class Department
{
    /**
     * 基类服务对象
     * @var \deepseath\hris\Hris
     */
    protected $service = null;

    /**
     * 部门删除状态：正常（未删除）
     * @var integer
     */
    const DELETE_NO = 1;
    /**
     * 部门删除状态：已删除
     * @var integer
     */
    const DELETE_YES = 2;

    /**
     * 畅移本地顶级部门（智慧零售部）的 id
     * md5('2022-04-27 17:00:01') —— 该值没具体意义主要就是一个在hris识别“智慧零售事业部”的标识<br>
     * 该值的来源是，首次导给 hris 全量部门数据时，以当时时间md5做顶级部门标识，方便 hris 导入处理
     * @var string
     */
    const CY_TOP_ID = '6e691aaf7d16f7b5199cce086e781368';

    public function __construct(\deepseath\hris\Hris $service)
    {
        $this->service = $service;
    }

    /**
     * 获取部门信息
     * @desc 查询HRIS中的部门基础信息
     * @param array $params
     * <pre>
     *  orgUnitZH       String  一级部门(中文)
     *  buFunctionZH    String  二级部门(中文)
     *  departmentZH    String  三级部门(中文)
     *  teamZH          String  四级部门(中文)
     * </pre>
     * @return array
     * <strong>map</strong>
     * <pre>
     *  orgUnitId     string  一级部门id
     *  orgUnitZH     string  一级部门名称中文
     *  orgUnitEN     string  一级部门名称英文
     *  buFunctionId  string  二级部门id
     *  buFunctionZH  string  二级部门名称中文
     *  buFunctionEN  string  二级部门名称英文
     *  departmentId  string  三级部门id
     *  departmentZH  string  三级部门名称中文
     *  departmentEN  string  三级部门名称英文
     *  teamId        string  四级部门id
     *  teamZH        string  四级部门名称中文
     *  teamEN        string  四级部门名称英文
     * </pre>
     */
    public function branchList(array $params) : array
    {
        $params = array_merge([
            //'orgUnitZH' => '',
            //'buFunctionZH' => '',
            //'departmentZH' => '',
            //'teamZH' => ''
        ], $params);

        $result = $this->service->apiRequest('get', 'branchList', $params);
        return isset($result['branchList']) ? $result['branchList'] : (isset($result['data']) ? $result['data'] : []);
    }

    /**
     * 新增OR更新部门
     * @desc 新增OR更新HRIS中的部门
     * @param array $params
     * <pre>
     *  branchNo        String  畅移部门ID
     *  parentBranchNo  String  畅移上级部门ID
     *  nameZH          String  部门名称(中文)
     *  nameEN          String  部门名称(英文)
     * </pre>
     * @return array
     * <pre>
     *  affectRow  string  受影响的行数
     *  branchId   string  hris中的部门ID
     * </pre>
     */
    public function branchAddOrUpdate(array $params) : array
    {
        $params = array_merge([
            //'branchNo' => '',
            //'parentBranchNo' => '',
            //'nameZH' => '',
            //'nameEN' => '',
            'deleted' => self::DELETE_NO
        ], $params);
        if ($params['parentBranchNo'] === '') {
            // 如果父级部门 id 不存在，则认为更新的是畅移的一级部门（在 hris 是属于“智慧零售事业部”下的二级部门）
            // 使用虚拟的“智慧零售部” id 做为父级 id
            $params['parentBranchNo'] = self::CY_TOP_ID;
        }

        $result = $this->service->apiRequest('post', 'branchAddOrUpdate', $params);

        return $result;
    }

    /**
     * 删除部门
     * @desc 删除HRIS中的部门
     * @param array $params
     * <pre>
     *  branchNo        String  畅移部门ID
     *  parentBranchNo  String  畅移上级部门ID
     *  nameZH          String  部门名称(中文)
     *  nameEN          String  部门名称(英文)
     * </pre>
     * @return array
     * <pre>
     *  affectRow  string  受影响的行数
     *  branchId   string  hris中的部门ID
     * </pre>
     */
    public function branchDelete(array $params) : array
    {
        $params = array_merge([
            'branchNo' => '',
            'parentBranchNo' => '',
            'nameZH' => '',
            'nameEN' => '',
            'deleted' => self::DELETE_YES
        ], $params);
        return $this->branchAddOrUpdate($params);
    }
}
