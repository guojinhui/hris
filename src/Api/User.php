<?php
/**
 * User.php
 * 用户接口
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\hris\Api;

class User
{
    /**
     * 基类服务对象
     * @var \deepseath\hris\Hris
     */
    protected $service = null;

    public function __construct(\deepseath\hris\Hris $service)
    {
        $this->service = $service;
    }

    /**
     * 员工基础信息
     * @desc 查询 HRIS 中的员工基础信息
     * @param array $params
     * @return array
     * <pre>
     *  list
     *   employeeId
     *   nameZH
     *   nameEN
     *   email
     *   wxUserId
     *   positionId
     * </pre>
     */
    public function baseList(array $params) : array
    {
        if (empty($params)) {
            $params = array_merge([
                'nameZH' => '',
                'nameEN' => '',
                //'email' => ''
            ], $params);
            $params = [];
        }

        $result = $this->service->apiRequest('get', 'employeeList', $params);
        return isset($result['employeeList']) ? $result['employeeList'] : (isset($result['data']) ? $result['data'] : []);
    }

    /**
     * 新增员工
     * @desc 向HRIS中插入新用户
     * @param array $params
     * @return array
     */
    public function add(array $params) : array
    {
        $this->_fieldCheck($params);
        if (!isset($params['orgUnitZH'])) {
            $params['orgUnitZH'] = $this->service->config['orgUnitZH'];
        }
        if (!isset($params['orgUnitEN'])) {
            $params['orgUnitEN'] = $this->service->config['orgUnitEN'];
        }
        return $this->service->apiRequest('post', 'employeeAdd', $params);
    }

    /**
     * 更新员工
     * @desc 用户修改
     * @param array $params
     * @return array
     */
    public function update(array $params) : array
    {
        $this->_fieldCheck($params);
        if (!isset($params['employeeId']) || empty($params['employeeId'])) {
            // 如果未提供员工 HRIS ID，则尝试查询
            $result = $this->baseList([
                'nameZH' => $params['nameZH'],
                'nameEN' => $params['nameEN'],
                'email' => $params['email']
            ]);
            if (empty($result)) {
                throw new \Exception('Hris SDK: 更新员工失败，无法查找员工 ID', 91002);
            }
            if (count($result) > 1) {
                throw new \Exception('Hris SDK: 更新员工失败，无法确定员工 ID', 91003);
            }
            $params['employeeId'] = $result[0]['employeeId'];
        }

        return $this->service->apiRequest('post', 'employeeUpdate', $params);
    }

    /**
     * 员工离职
     * @param string $employeeId
     * @return array
     */
    public function dismiss(string $employeeId) : array
    {
        $params = [
            'employeeId' => $employeeId,
            'employeeStatus' => self::STATUS_OFFLINE
        ];
        return $this->service->apiRequest('post', 'employeeUpdate', $params);
    }

    /**
     * 员工数据参数检查
     * @param array $params
     * @throws \Exception
     * @return bool
     */
    private function _fieldCheck(array $params) : bool
    {
        // 必须提供的参数
        /*
        $requires = [
            'nameZH', // *员工姓名(中文)
            'nameEN', // *员工姓名(英文)
            'entityId', // *法务实体ID(畅移上海157 . 畅移西安 158)
            //'orgUnitZH', // *一级部门（中文）
            //'orgUnitEN', // *一级部门（英文）
            'locationId', // *工作地点ID  上海65 西安 116
            'employeeType', // *员工类型（全职为：Full Time  Employee、 需要转义，请看转义说明）
            'positionZH', // *职位名称（中文）
            'positionEN', // *职位名称（英文）
            'gradeId', // *职级ID （需要转义，如不填核对grade名称）
            'bizLeader', // *业务汇报线
            'pmLeader', // *直接汇报线
            'certificateType', // *证件类型
            'certificateNumber', // *证件号
            'highestEducationId', // *最高学历 （需要转义，如不填需要核对highestEducation名称）
            'major', // *主修专业
            'schoolStartDate', // *学校开始学习日期
            'schoolEndDate', // *结束学习日期
            'mobile', // *手机号
            'email', // *公司邮箱
            'startWorkDate', // *开始工作日期
            'contractStartDate', // *合同开始时间
            'probationEndDate', // *试用期结束时间
        ];
        foreach ($requires as $_key) {
            if (!isset($params[$_key]) || !is_scalar($params[$_key])) {
                throw new \Exception('Add user lose params "'.$_key.'"', 91001);
            }
        }
        */

        return true;
    }

    /**
     * 在职状态：在职
     */
    const STATUS_ON = '1';
    /**
     * 在职状态：离职
     */
    const STATUS_OFFLINE = '3';
    /**
     * 在职状态描述映射关系
     * @var array
     */
    const MAP_STATUS = [
        self::STATUS_ON => '在职',
        self::STATUS_OFFLINE => '离职'
    ];

    /**
     * 法务实体映射关系
     * @var array
     */
    const MAP_ENTOTY = [
        '157' => '畅移（上海）信息科技有限公司',
        '158' => '西安畅展信息科技有限公司',
    ];

    /**
     * 工作地映射关系
     * @var array
     */
    const MAP_LOCATION = [
        '65' => '上海',
        '116' => '西安',
    ];

    /**
     * 员工类型映射关系
     * @var array
     */
    const MAP_TYPE = [
        '0' => '请选择',
        '1' => 'Full Time Employee',// 全职
        '2' => 'Intern',// 实习生
        '3' => 'Consultant',// 顾问
        '4' => 'Others on Payroll',// 编外
        '5' => 'Temp',// 临时工
        '6' => 'Part Time Employee',// 兼职
        '7' => '其他',
    ];

    /**
     * 职级映射关系
     * @var array
     */
    const MAP_GRADE_ID = [
        '179' => ['E2', 'Sr. Executive'],
        '181' => ['E1', 'Executive'],
        '183' => ['M4', 'GM/VP'],
        '187' => ['M3', 'Director'],
        '189' => ['IC6', 'Director'],
        '191' => ['M2', 'Sr. Manager'],
        '193' => ['IC5', 'Sr. Manager'],
        '195' => ['M1', 'Manager'],
        '197' => ['IC4', 'Manager'],
        '199' => ['IC3', 'Associate Manager'],
        '201' => ['IC2', 'Sr. Specialist'],
        '203' => ['IC1', 'Specialist'],
        '205' => ['S2', 'Coordinator'],
        '207' => ['S1', 'Assistant'],
        '209' => ['E1', 'COO'],
        '213' => ['IC7', 'GM/VP'],
        '315' => ['E1', 'CFO'],
        '316' => ['E3', 'Chairman'],
        '319' => ['M5', 'GM/VP']
    ];

    /**
     * 身份证件类型映射关系
     * @var array
     */
    const MP_CERTIFICATE_TYPE = [
        '0' => '请选择',
        '1' => '身份证',
        '2' => '护照',
        '3' => '台胞证',
        '4' => '身份证（香港）',
        '5' => '身份证（新加坡）',
        '6' => '港澳通行证',
    ];

    /**
     * 婚姻状态映射关系
     * @var array
     */
    const MP_MARITAL_STATUS = [
        '0' => '请选择',
        '1' => '未婚',
        '2' => '已婚',
    ];

    /**
     * 最高学历映射关系
     * @var array
     */
    const MAP_HIGHEST_EDUCATION = [
        '0' => ['请选择', ''],
        '95' => ['PhD', '博士'],
        '97' => ['Master', '硕士'],
        '99' => ['Bachelor', '本科'],
        '103' => ['Senior High', '高中'],
        '105' => ['Junior High', '初中'],
        '107' => ['Diploma', '大专'],
        '109' => ['Associate Degree', '大学肆业'],
        '111' => ['Advanced Diploma', '高级文凭'],
        '113' => ['Higher Diploma', '高级文凭'],
        '115' => ['Juris Doctor', '法学博士'],
        '117' => ['Certificate', '证书'],
        '119' => ['Secondary ', '中专'],
        '121' => ['F.5', 'F.5'],
    ];

    /**
     * 民族映射关系
     * @var array
     */
    const NATION = [
        '0' => '请选择',
        '1' => '汉族',
        '10' => '瑶族',
        '11' => '锡伯族',
        '12' => '苗族',
        '2' => '蒙古族',
        '3' => '土家族',
        '4' => '满族',
        '5' => '壮族',
        '6' => '仫佬族',
        '7' => '回族',
        '8' => '英国',
        '9' => '维吾尔族',
    ];

    /**
     * 国籍映射关系
     * @var array
     */
    const MAP_NATION_NALITY = [
        '0' => '请选择',
        '1' => '中国',
        '2' => '美国',
        '3' => '新加坡',
        '5' => '加拿大',
        '7' => '英国',
        '9' => '泰国',
        '11' => '日本',
        '13' => '韩国',
        '15' => '法国',
        '17' => '香港',
        '19' => '台湾',
        '21' => '澳大利亚',
        '23' => '菲律宾',
        '24' => '德国',
        '25' => '意大利',
        '26' => '马来西亚',
        '27' => '新西兰',
        '28' => '西班牙',
        '29' => '其他',
    ];

    /**
     * 角色映射关系
     * @var array
     */
    const MAP_JOB_ROLE = [
        '0' => '请选择',
        '10' => 'Engineering',
        '12' => 'Finance',
        '14' => 'Human Resources',
        '16' => 'Management',
        '17' => 'Marketing',
        '18' => 'Media',
        '19' => 'Optimization',
        '20' => 'Product Management',
        '21' => 'Project Management',
        '23' => 'Sales',
        '28' => 'Creative Design',
        '29' => 'Legal',
        '3' => 'Account Management',
        '30' => 'Product Marketing',
        '31' => 'Product Trainer',
        '32' => 'Sales Operations',
        '37' => 'Solution Planning',
        '4' => 'Accounting',
        '40' => 'Biz Planning & Operations',
        '41' => 'Solution Development',
        '43' => 'Product Operations',
        '44' => 'User Experience Design',
        '45' => 'Public Relations',
        '46' => 'Data Analysis',
        '47' => 'Kol Operations',
        '48' => 'Content Operations',
        '49' => 'Creative',
        '5' => 'Ad. Operations',
        '6' => 'Administration',
        '8' => 'Biz Development',
        '9' => 'Biz Intelligence',
    ];

    /**
     * 性别映射关系
     * @var array
     */
    const MAP_GENDER = [
        '0' => '未填写',
        '1' => '先生',
        '2' => '女士'
    ];
}
